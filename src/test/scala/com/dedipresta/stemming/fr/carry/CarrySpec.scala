package com.dedipresta.stemming.fr.carry

import org.junit.runner.RunWith
import org.specs2.mutable.Specification
import org.specs2.runner.JUnitRunner

@RunWith(classOf[JUnitRunner])
object CarrySpec extends Specification {

  val refs = Seq(
    ("abondes", "abond"),
    ("abusif", "abusiv")
  )

  "Carry" should {
    "stem properly words" in {
      refs.forall {
        ref => Carry.applyRules(ref._1) must beEqualTo(ref._2)
      } must beTrue
    }
  }
}
