package com.dedipresta.stemming.fr.paicehusk

import java.util.regex.Pattern

import scala.annotation.tailrec

object PaiceHusk {

  /**
    * Pattern to format rule
    */
  private val rulePattern = Pattern.compile("^([a-zàâèéêëîïôûùç]*)(\\*){0,1}(\\d)([a-zàâèéêëîïôûùç]*)([.|>])")

  /**
    * Match vowels
    */
  private val vowelsPatternWithEnd = Pattern.compile("[aàâeèéêëiîïoôuûùy]$")

  /**
    * Match vowels
    */
  private val vowelsPatternWithoutEnd = Pattern.compile("[aàâeèéêëiîïoôuûùy]")

  /**
    * Entry point to apply stemming algo to word
    * @param word the word to stem
    * @return the stemmed word
    */
  def applyRules(word: String): String = stem(word.reverse, 0).reverse

  /**
    * Apply rules
    * @param reversed the word to stem with reversed letters
    * @param ruleIndex the last applied rule index
    * @return stemmed word
    */
  @tailrec
  private def stem(reversed: String, ruleIndex: Int): String = {
    val newRuleIndex = getFirstRule(reversed, ruleIndex)
    if (newRuleIndex == -1) {
      reversed
    } else {
      val matcher = rulePattern.matcher(stemRules(newRuleIndex))
      if (matcher.matches()) {
        if (matcher.group(2) == null || matcher.group(2) != "*") {
          val reverseStem = matcher.group(4) + reversed.substring(matcher.group(3).toInt)
          if (checkAcceptability(reverseStem)) {
            if (matcher.group(5) == ".") reverseStem else stem(reverseStem, newRuleIndex)
          } else stem(reversed, newRuleIndex + 1)
        } else stem(reversed, newRuleIndex + 1)
      } else stem(reversed, newRuleIndex + 1)
    }
  }

  /**
    * Get the index from the first matching rule
    * @param reversed the word to stem in reversed order
    * @param start last rule index
    * @return the index to the rule to apply
    */
  private def getFirstRule(reversed: String, start: Int): Int = (start until stemRules.length) find { i: Int =>
    val newRule = regexpMatchAndReplace(stemRules(i), rulePattern, "$1")
    reversed.startsWith(newRule)
  } getOrElse -1

  /**
    * Replace substring for stemming
    * @param text the text to stem
    * @param pattern the pattern for replacement
    * @param replace the string to replace with
    * @return the stemmed word
    */
  private def regexpMatchAndReplace(text: String, pattern: Pattern, replace: String): String = {
    pattern.matcher(text).replaceAll(replace)
  }

  /**
    * Check if pattern is found in word
    * @param text the word to check
    * @param pattern the pattern to match
    * @return true if found
    */
  private def regexpFind(text: String, pattern: Pattern): Boolean = pattern.matcher(text).find()

  /**
    * Check if the stemming should continue
    * @param reversed the reversed word
    * @return true if algo is done
    */
  private def checkAcceptability(reversed: String): Boolean = {
    if (regexpFind(reversed, vowelsPatternWithEnd)) { // if form starts with vowel, at least two letters must remain
      reversed.length > 2
    } else if (reversed.length() <= 2) { // if form starts with a consonant, at least two letters must remain
      false
    } else { // and at least one of these must be a vowel
      regexpFind(reversed, vowelsPatternWithoutEnd)
    }
  }

  /**
    * Load rules to apply (from external file)
    */
  private lazy val stemRules = {
    import java.io.InputStream

    val stream: InputStream = getClass.getResourceAsStream("/paicehusk")
    val source = scala.io.Source.fromInputStream(stream)
    val lines = (source.getLines map formatImportedRules).toArray // toArray to consume the stream
    source.close()
    stream.close()
    lines
  }

  /**
    * Format a line from db rules to extract rule
    * @param line the line to format to rule
    * @return the rule
    */
  private def formatImportedRules(line: String): String = line.split('#').head
}
