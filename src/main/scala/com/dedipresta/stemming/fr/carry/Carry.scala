package com.dedipresta.stemming.fr.carry



object Carry {

  private lazy val rules1: Seq[CarryRule] = loadRules("r1")
  private lazy val rules2: Seq[CarryRule] = loadRules("r2")
  private lazy val rules3: Seq[CarryRule] = loadRules("r3")

  private def step(rules: Seq[CarryRule], s: String): String = rules.find { rule => s.endsWith(rule.pattern) } match {
    case Some(rule: CarryRule) => Carry.replaceLast(s, rule.pattern, rule.replace)
    case None => s
  }

  private def step1(s: String): String = step(rules1, s)
  private def step2(s: String): String = step(rules2, s)
  private def step3(s: String): String = step(rules3, s)

  private def replaceLast(s: String, toReplace: String, replacement: String): String = {
    val pos = s.lastIndexOf(toReplace)
    if (pos > -1) s.substring(0, pos) + replacement + s.substring(pos + toReplace.length, s.length) else s
  }

  /**
    * Load rules to apply (from external file)
    */
  private def loadRules(rules: String): Seq[CarryRule] = {
    import java.io.InputStream

    val stream: InputStream = getClass.getResourceAsStream("/carry/" + rules)
    val source = scala.io.Source.fromInputStream(stream)
    val lines = (source.getLines map formatImportedRules).toList // toList to consume the stream
    source.close()
    stream.close()
    lines
  }

  /**
    * Format a line from db rules to extract rule
    *
    * @param line the line to format to rule
    * @return the rule
    */
  private def formatImportedRules(line: String): CarryRule = {
    line split ',' match {
      case Array(p, r) => CarryRule(p, r)
      case Array(p) => CarryRule(p, "")
    }
  }

  /**
    * Apply rules on a word
    * @param word the word to stem
    * @return the stemmed word
    */
  def applyRules(word: String): String = step3(step2(step1(word)))
}
