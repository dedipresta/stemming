package com.dedipresta.stemming.fr.carry

case class CarryRule(pattern: String, replace: String)
