# Stemming algorithm 1.0.1

## How to use

### Carry
```scala
Carry.applyRules("bonjour")
```

### Paice and Husk
```scala
PaiceHusk.applyRules("bonjour")
```
    
## Tests
Run tests and coverage with **sbt**

    1. sbt clean coverage test
    2. sbt coverageReport
