name := "stemming"

organization := "com.dedipresta"

version := "1.0.1"

scalaVersion := "2.12.5"

crossScalaVersions := Seq("2.11.11", "2.12.1")

coverageEnabled := true

val specs2Version = "3.9.0"

// Read here for optional jars and dependencies
libraryDependencies ++= Seq(
  "org.specs2" %% "specs2-core" % specs2Version % "test",
  "org.specs2" %% "specs2-junit" % specs2Version % "test"
)

scalacOptions in Test ++= Seq("-Yrangepos")

